# test-devops
This is packer - ansible solution for deploy open-ssh and iptables with open 22 port on EC2 AWS instances.

!! Please note -this is beta version and not tested on real EC2 instance.
This code requires making the final tuning after testing on real instances.


Required.

goss v0.3.6
ansible 2.5.1
packer 1.4.4
packer-provisioner-goss  (https://github.com/YaleUniversity/packer-provisioner-goss)

How to run packer:
###############################
Before running need to be specified in the ami-test.json
the following parameters:

    "access_key":"",
    "secret_key":"",
    "aws_vpc_id": "",
    "aws_subnet_id": ""
###############################

Run:

packer build packer/ami-test.json

##############################
How to run test using GOSS

goss -g goss/goss.yaml validate

##############################
Terraform

1. Configure your AWS credentials using one of the supported methods for AWS CLI tools, such as setting the
AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY environment variables. If you're using the ~/.aws/config file for
profiles then export AWS_SDK_LOAD_CONFIG as "True".
2. Install Terraform and make sure it's on your PATH.
3. Run terraform init.
4. Run terraform apply.
5. When you're done, run terraform destroy.
